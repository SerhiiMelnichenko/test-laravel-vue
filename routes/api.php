<?php

use Illuminate\Support\Facades\Route;

Route::prefix('links')->namespace('CustomLink')->group(function () {
    Route::get('/', 'CustomLinkController@getAll');
    Route::get('/{link}', 'CustomLinkController@getOne');
    Route::post('/', 'CustomLinkController@create');
});