<?php

namespace App\Providers;

use App\Services\CustomLink\ShortLink;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerLinkServices();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function registerLinkServices()
    {
        $this->app->bind('link-service', ShortLink::class);
    }
}
