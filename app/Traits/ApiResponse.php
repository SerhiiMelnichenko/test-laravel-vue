<?php


namespace App\Traits;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait ApiResponse
 * @package App\Traits
 *
 * todo add documentations
 */
trait ApiResponse
{
    protected function responseAll(Collection $collection, $code = 200)
    {
        return $this->successResponse(['data' => $collection], $code);
    }

    protected function responseOne(Model $model, $code = 200)
    {
        return $this->successResponse(['data' => $model], $code);
    }

    protected function responseError($message, $code)
    {
        return $this->successResponse(['error' => $message, 'code' => $code], $code);
    }

    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }
}