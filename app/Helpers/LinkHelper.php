<?php


namespace App\Helpers;


use App\CustomLink;
use App\Http\Requests\LinkRequest;

class LinkHelper
{
    public function saveToDB(LinkRequest $request, string $linkCode)
    {
        $link = new CustomLink();
        $link->origin_link = $request->get('link');
        $link->short_code = $linkCode;
        $link->generated_link = config('app.url', 'http://myapp') . '/' . $linkCode;
        $link->save();

        return $link;
    }

    public function getAll()
    {
        $links = CustomLink::all();//todo: add cache, transformer

        return $links;
    }

    public function getOne(CustomLink $link)
    {
        return $link;
    }
}