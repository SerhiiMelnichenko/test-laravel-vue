<?php

namespace App\Http\Controllers\CustomLink;

use App\CustomLink;
use App\Helpers\LinkHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\LinkRequest;
use App\Services\CustomLink\LinkBuilder;
use Illuminate\Http\JsonResponse;

/**
 * Class CustomLinkController
 * @package App\Http\Controllers\CustomLink
 */
class CustomLinkController extends Controller
{
    private $linkHelper;

    public function __construct()
    {
        $this->linkHelper = new LinkHelper();
    }

    /**
     * Create and save custom link
     *
     * @param LinkRequest $request
     * @return JsonResponse
     */
    public function create(LinkRequest $request)
    {
        $linkCode = (new LinkBuilder())->make(app('link-service'));

        $link = $this->linkHelper->saveToDB($request, $linkCode);

        return $this->responseOne($link, 201);
    }

    /**
     * @return JsonResponse
     */
    public function getAll()
    {
        $links = $this->linkHelper->getAll();

        return $this->responseAll($links);
    }

    /**
     * @param CustomLink $link
     * @return JsonResponse
     */
    public function getOne(CustomLink $link)
    {
        $link = $this->linkHelper->getOne($link);

        return $this->responseOne($link);
    }
}
