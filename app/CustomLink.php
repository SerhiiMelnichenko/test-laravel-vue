<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomLink
 * @package App
 *
 * @property string origin_link
 * @property string short_code
 * @property string generated_link
 */
class CustomLink extends Model
{
    protected $fillable = [
        'origin_link', 'short_code', 'generated_link'
    ];
}
