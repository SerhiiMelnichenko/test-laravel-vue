<?php


namespace App\Services\CustomLink;


use Illuminate\Support\Str;

class ShortLink implements LinkBuilderInterface
{
    /**
     * Create custom link
     *
     * @return string
     */
    public function makeLink(): string
    {
        return Str::random(5);
    }
}