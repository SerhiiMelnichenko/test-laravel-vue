<?php


namespace App\Services\CustomLink;


/**
 * Interface LinkBuilderInterface
 * @package App\Services\CustomLink
 */
interface LinkBuilderInterface
{
    /**
     * Create custom link
     *
     * @return string
     */
    public function makeLink(): string;
}