<?php


namespace App\Services\CustomLink;


class LinkBuilder
{
    public function make(LinkBuilderInterface $linkBuilder)
    {
        $link = $linkBuilder->makeLink();

        return $link;
    }
}